# Salesforce Analytics Cloud Writer #

Salesforce Analytics Cloud Writer component for Keboola Connection.

### Functionality ###

The component takes files from in/tables directory and overwrite data in your Analytics Cloud. Name of object, which will be updated, is taken from the file name (e.g. account.csv will update object Account). CSV file has to have a header with field names, which have to exists in Salesforce. ID column is used to identify record which will be updated. When inserting records all required fields has to be filled in. 

The files must be in UTF-8 format.

Only one inout table per dataset is supported.

### Configuration ###
#### Parameters ####

* Loginname - (REQ) your user name, when updating data in sandbox don't forget to add .sandboxname at the end
* Password - (REQ) your password
* Security Token - (REQ) your security token, don't forget it is different for sandbox
* Dataset - (REQ) data set name in Analytics Cloud
* sandbox - (REQ) true when you want to update data in sandbox

package keboola.salesforcewave.writer;

import java.io.*;
import java.util.*;

import com.sforce.async.*;
import com.sforce.soap.partner.sobject.*;
import com.sforce.soap.partner.*;
import com.sforce.ws.*;

import keboola.salesforcewave.writer.config.JsonConfigParser;
import keboola.salesforcewave.writer.config.KBCConfig;

import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author David Esner <esnerda at gmail.com>
 * @author Martin Humpolec <martin.humpolec at gmail.com>
 * @created 2017
 */
public class Writer {
	//
	int part = 1;

	public static void main(String[] args) throws AsyncApiException, ConnectionException, IOException   {
		if (args.length == 0) {
			System.err.println("No parameters provided.");
			System.exit(1);
		}

		String dataPath = args[0];
		String inTablesPath = dataPath + File.separator + "in" + File.separator + "tables" + File.separator;

		KBCConfig config = null;
		File confFile = new File(args[0] + File.separator + "config.json");
		if (!confFile.exists()) {
			System.out.println("config.json does not exist!");
			System.err.println("config.json does not exist!");
			System.exit(1);
		}
		// Parse config file
		try {
			if (confFile.exists() && !confFile.isDirectory()) {
				config = JsonConfigParser.parseFile(confFile);
			}
		} catch (Exception ex) {
			System.out.println("Failed to parse config file");
			System.err.println(ex.getMessage());
			System.exit(1);
		}
		if (!config.validate()) {
			System.out.println(config.getValidationError());
			System.err.println(config.getValidationError());
			System.exit(1);
		}

		System.out.println( "Everything ready, write to Salesforce, loginname: " + config.getParams().getLoginname());
		Writer sfupd = new Writer();
		sfupd.runUpload(config.getParams().getLoginname(),
				config.getParams().getPassword() + config.getParams().getSecuritytoken(), inTablesPath, config.getParams().getSandbox(), config.getParams().getDataset());

		System.out.println( "All done");
	}


	/**
	 * Creates an API job and uploads batches for a CSV file.
	 */
	public void runUpload(String userName, String password, String filesDirectory, boolean sandbox, String dataSet)
			throws AsyncApiException, ConnectionException, IOException {
		PartnerConnection connection = getConnection(userName, password, sandbox);

		System.out.println( "Create import");
		com.sforce.soap.partner.sobject.SObject sobj = new com.sforce.soap.partner.sobject.SObject();
		sobj.setType("InsightsExternalData"); 
		sobj.setField("Format","Csv");
		sobj.setField("EdgemartAlias", dataSet);
		//		sobj.setField("MetadataJson",metadataJson);
		sobj.setField("Operation","Overwrite");
		sobj.setField("Action","None");

		String parentID = "";

		SaveResult[] results = connection.create(new com.sforce.soap.partner.sobject.SObject[] { sobj });

		for(SaveResult sv:results)
			if(sv.isSuccess())
				parentID = sv.getId();

		File folder = new File( filesDirectory);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				String fileName = listOfFiles[i].getName().toString();
				int position = fileName.indexOf('.');
				String fileNameShort = fileName.substring( 0, position);
				boolean manifest = fileName.endsWith( "manifest");
				if( manifest == false) {
					System.out.println( "found file " + fileName + ", object " + fileNameShort);

					createBatchesFromCSVFile(connection, filesDirectory + listOfFiles[i].getName(), parentID);
				}
			}
		}    	
		closeJob(connection, parentID);
		awaitCompletion(connection, parentID);
	}



	private void closeJob(PartnerConnection connection, String parentID) {
		try {
			System.out.println( "Closing job " + parentID);
			com.sforce.soap.partner.sobject.SObject sobj = new com.sforce.soap.partner.sobject.SObject();
			sobj.setType("InsightsExternalData"); 
			sobj.setField("Action","Process");
			sobj.setId(parentID); // This is the rowID from the previous example.
			SaveResult[] results = connection.update(new com.sforce.soap.partner.sobject.SObject[] { sobj });

			String rowId;
			for(SaveResult sv:results)
				if(sv.isSuccess()) 
					rowId = sv.getId();
			System.out.println( "Job closed");

		} catch( Exception e){
			System.err.println( "Error in closeJob " + e.getMessage());
		}
	}


	/**
	 * Wait for a job to complete by polling the API.
	 */
	private void awaitCompletion(PartnerConnection connection, String parentId) throws AsyncApiException
	{
		String status = null;
		System.out.println( "Waiting ");
		try {
			for(int a=1; a<60000; a++) {
				//			Thread.sleep(i==0 ? 30 * 1000 : 30 * 1000); //30 sec

				Thread.sleep(a<30 ? a * 1000 : 30 * 1000); //30 sec
				String soqlQuery = String.format("SELECT Status, StatusMessage FROM InsightsExternalData WHERE Id = '%s'", parentId);
				connection.setQueryOptions(2000);
				QueryResult qr = connection.query(soqlQuery);
				if (qr.getSize() > 0) 
				{
						com.sforce.soap.partner.sobject.SObject[] records = qr.getRecords();
//						System.out.println( "Get record");
						for (int i = 0; i < records.length; ++i) 
						{
							status = (String) records[i].getField("Status");
							String statusMessage = (String) records[i].getField("StatusMessage");
							System.out.println( "Status: " + status + ", " + statusMessage);

							if (status.equals( "Failed")) {
								System.err.println( "Status: " + status + ", " + statusMessage);
								System.exit(1);
								break;
							} else if ( status.equals( "Completed") || status.equals( "CompletedWithWarnings")) {
								System.out.println( "Status: " + status + ", " + statusMessage);
								break;
							}
						}
				}
				if( status.equals( "Completed")  || status.equals( "CompletedWithWarnings") || status.equals( "Failed")) { break; }
			}

		} catch( Exception e){
			System.out.println( "Exception " + e);
		}
		System.out.println( "After waiting");
	}

	/**
	 * Create the Connection used to call API operations.
	 */
	private PartnerConnection getConnection(String userName, String password, boolean sandbox)
			throws ConnectionException {
		ConnectorConfig partnerConfig = new ConnectorConfig();
		partnerConfig.setUsername(userName);
		partnerConfig.setPassword(password);
		if ( sandbox == true)  {
			System.out.println("Connecting to Salesforce Sandbox");
			partnerConfig.setAuthEndpoint("https://test.salesforce.com/services/Soap/u/39.0");
		} else {
			System.out.println("Connecting to Salesforce Production");
			partnerConfig.setAuthEndpoint("https://login.salesforce.com/services/Soap/u/39.0");			
		}
		// Creating the connection automatically handles login and stores
		// the session in partnerConfig
		PartnerConnection connection = new PartnerConnection(partnerConfig);
		return connection;
	}

	/**
	 * Create and upload batches using a CSV file. The file into the appropriate
	 * size batch files.
	 */
	private void createBatchesFromCSVFile(PartnerConnection connection, String csvFileName, String parentId)
			throws IOException {
		System.out.println( "Creating batches");
		BufferedReader rdr = new BufferedReader(new InputStreamReader(new FileInputStream(csvFileName)));
		// read the CSV header row
		byte[] headerBytes = (rdr.readLine() + "\n").getBytes("UTF-8");
		int headerBytesLength = headerBytes.length;
		File tmpFile = File.createTempFile("APIUpdate", ".csv");

		// Split the CSV file into multiple batches
		try {
			FileOutputStream tmpOut = new FileOutputStream(tmpFile);
			int maxBytesPerBatch = 10000000; // 10 million bytes per batch
			int maxRowsPerBatch = 10000; // 10 thousand rows per batch
			int currentBytes = 0;
			int currentLines = 0;
			String nextLine;
			while ((nextLine = rdr.readLine()) != null) {
				byte[] bytes = (nextLine + "\n").getBytes("UTF-8");
				// Create a new batch when our batch size limit is reached
				if (currentBytes + bytes.length > maxBytesPerBatch || currentLines > maxRowsPerBatch) {
					createBatch(tmpOut, tmpFile, connection, parentId, part);
					currentBytes = 0;
					currentLines = 0;
					part++;
				}
				if (currentBytes == 0) {
					tmpOut = new FileOutputStream(tmpFile);
					tmpOut.write(headerBytes);
					currentBytes = headerBytesLength;
					currentLines = 1;
				}
				tmpOut.write(bytes);
				currentBytes += bytes.length;
				currentLines++;
			}
			// Finished processing all rows
			// Create a final batch for any remaining data
			if (currentLines > 1) {
				createBatch(tmpOut, tmpFile, connection, parentId, part);
				part++;
			}
		} finally {
			tmpFile.delete();
		}

	}

	/**
	 * Create a batch by uploading the contents of the file. This closes the
	 * output stream.
	 */
	private void createBatch(FileOutputStream tmpOut, File tmpFile, 
			PartnerConnection connection, String parentID, Integer part) throws IOException {
		System.out.println( "Create specific batch");
		tmpOut.flush();
		tmpOut.close();
		FileInputStream tmpInputStream = new FileInputStream(tmpFile);
		try {
			System.out.println( "Creating data part " + part);
			com.sforce.soap.partner.sobject.SObject sobj = new com.sforce.soap.partner.sobject.SObject();
			sobj.setType("InsightsExternalDataPart"); 
			byte[] bytes = new byte[(int)tmpFile.length()];
			tmpInputStream.read(bytes);
            String encodedBase64 = new String(Base64.encodeBase64(bytes));
//			sobj.setField("DataFile", encodedBase64);
			sobj.setField("DataFile", bytes);
					sobj.setField("InsightsExternalDataId", parentID);
			sobj.setField("PartNumber",part); //Part numbers should start at 1

			String rowId = null;

			SaveResult[] results = connection.create(new com.sforce.soap.partner.sobject.SObject[] { sobj });
			for(SaveResult sv:results){
				if(sv.isSuccess()) {
					rowId = sv.getId();
			    } else {
				    for(com.sforce.soap.partner.Error err : sv.getErrors()) {
		            	System.err.println("The following error has occurred.");                   
        		    	System.err.println(err.getStatusCode() + ": " + err.getMessage());
					}	
        		}
			}
			System.out.println( "Data part created " + rowId);

		} catch( Exception e){
			System.err.println( "Error in createBatch " + e.getMessage());
		} finally {
			tmpInputStream.close();
		}
	}
}